package entities;

import entities.Casovi;
import entities.Ocene;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-06T15:00:08")
@StaticMetamodel(Predmeti.class)
public class Predmeti_ { 

    public static volatile SingularAttribute<Predmeti, String> ime;
    public static volatile ListAttribute<Predmeti, Ocene> oceneList;
    public static volatile SingularAttribute<Predmeti, Integer> id;
    public static volatile ListAttribute<Predmeti, Casovi> casoviList;

}