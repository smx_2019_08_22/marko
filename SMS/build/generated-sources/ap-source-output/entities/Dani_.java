package entities;

import entities.Casovi;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-06T15:00:08")
@StaticMetamodel(Dani.class)
public class Dani_ { 

    public static volatile SingularAttribute<Dani, String> ime;
    public static volatile SingularAttribute<Dani, Integer> id;
    public static volatile ListAttribute<Dani, Casovi> casoviList;

}