package entities;

import entities.Korisnici;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-06T15:00:08")
@StaticMetamodel(Direktori.class)
public class Direktori_ { 

    public static volatile SingularAttribute<Direktori, String> ime;
    public static volatile SingularAttribute<Direktori, String> prezime;
    public static volatile SingularAttribute<Direktori, Integer> id;
    public static volatile SingularAttribute<Direktori, Korisnici> korisniciId;

}