package entities;

import entities.Dani;
import entities.Odeljenja;
import entities.Predmeti;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-06T15:00:08")
@StaticMetamodel(Casovi.class)
public class Casovi_ { 

    public static volatile SingularAttribute<Casovi, Integer> broj;
    public static volatile SingularAttribute<Casovi, Integer> id;
    public static volatile SingularAttribute<Casovi, Dani> daniId;
    public static volatile SingularAttribute<Casovi, Odeljenja> odeljenjaId;
    public static volatile SingularAttribute<Casovi, Predmeti> predmetiId;

}