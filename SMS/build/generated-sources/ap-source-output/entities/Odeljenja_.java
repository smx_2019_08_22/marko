package entities;

import entities.Casovi;
import entities.Ucenici;
import entities.Ucitelji;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-06T15:00:08")
@StaticMetamodel(Odeljenja.class)
public class Odeljenja_ { 

    public static volatile SingularAttribute<Odeljenja, String> ime;
    public static volatile ListAttribute<Odeljenja, Ucenici> uceniciList;
    public static volatile SingularAttribute<Odeljenja, Ucitelji> uciteljiId;
    public static volatile SingularAttribute<Odeljenja, Integer> id;
    public static volatile ListAttribute<Odeljenja, Casovi> casoviList;

}