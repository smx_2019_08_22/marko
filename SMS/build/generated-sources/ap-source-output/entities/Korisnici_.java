package entities;

import entities.Administratori;
import entities.Direktori;
import entities.Roditelji;
import entities.Ucitelji;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-06T15:00:08")
@StaticMetamodel(Korisnici.class)
public class Korisnici_ { 

    public static volatile SingularAttribute<Korisnici, String> lozinka;
    public static volatile ListAttribute<Korisnici, Direktori> direktoriList;
    public static volatile ListAttribute<Korisnici, Ucitelji> uciteljiList;
    public static volatile SingularAttribute<Korisnici, String> nalog;
    public static volatile ListAttribute<Korisnici, Roditelji> roditeljiList;
    public static volatile SingularAttribute<Korisnici, Integer> id;
    public static volatile ListAttribute<Korisnici, Administratori> administratoriList;
    public static volatile SingularAttribute<Korisnici, Integer> status;

}