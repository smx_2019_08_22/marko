package entities;

import entities.Korisnici;
import entities.Odeljenja;
import entities.Poruke;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-06T15:00:08")
@StaticMetamodel(Ucitelji.class)
public class Ucitelji_ { 

    public static volatile SingularAttribute<Ucitelji, String> ime;
    public static volatile SingularAttribute<Ucitelji, String> prezime;
    public static volatile SingularAttribute<Ucitelji, Integer> id;
    public static volatile ListAttribute<Ucitelji, Poruke> porukeList;
    public static volatile ListAttribute<Ucitelji, Odeljenja> odeljenjaList;
    public static volatile SingularAttribute<Ucitelji, Korisnici> korisniciId;

}