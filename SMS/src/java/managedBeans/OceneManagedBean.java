/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import business.sessionBeans.OceneSessionBeanLocal;
import business.sessionBeans.StudentSessionBeanLocal;
import entities.Ocene;
import entities.Ucenici;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Grupa1
 */
@ManagedBean(name = "oceneMB", eager = true)
@SessionScoped
public class OceneManagedBean implements Serializable{

    @EJB
    private OceneSessionBeanLocal oceneBean; 
    @EJB
    private StudentSessionBeanLocal studentBean; 
    
    private Integer ucenik;
    
    FacesContext facesContext = FacesContext.getCurrentInstance();
    StudentManagedBean studentMB = (StudentManagedBean)facesContext.getApplication()
      .createValueBinding("#{studentMB}").getValue(facesContext);
    
    public OceneManagedBean() {
        
    }
    public List<Ocene> getOceneUcenika(){ 
        Ucenici ucenici = studentBean.getStudentWhereId(Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("ucenik")));
        setUcenik(ucenici.getId());
        return oceneBean.getOceneUcenika(getUcenik());
    }
    public List<Ocene> getOceneWherePredmet(){
        Ucenici ucenici = studentBean.getStudentWhereId(Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("ucenik")));
        setUcenik(ucenici.getId());
        
        return oceneBean.getOceneWhereUcenikAndPredmet(getUcenik(), ucenik);
        
    }

    /**
     * @return the ucenik
     */
    public Integer getUcenik() {
        return ucenik;
    }

    /**
     * @param ucenik the ucenik to set
     */
    public void setUcenik(Integer ucenik) {
        this.ucenik = ucenik;
    }
    
    
}
