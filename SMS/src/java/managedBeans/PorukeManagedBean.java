/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import business.sessionBeans.PorukeSessionBeanLocal;
import entities.Poruke;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;

/**
 *
 * @author Grupa1
 */
@Named(value = "porukeMB")
@Dependent
public class PorukeManagedBean {

    @EJB
    private PorukeSessionBeanLocal porukeBean;
    
    public PorukeManagedBean() {
        
    }
    public String sendMessage(String text, int primalac,  int posiljalac){
        Poruke poruke = new Poruke();
        
        boolean result = porukeBean.sendMessage(poruke);
        return null;
        
    }
    
}
