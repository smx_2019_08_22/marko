package managedBeans;

import business.sessionBeans.RegisterSessionBeanLocal;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;


@ManagedBean(name = "registerMB", eager = true)
@SessionScoped
public class RegisterManagedBean implements Serializable {

    private String name;
    private String surname;
    private String username;
    private String password;
    private int user;
    
    private String message = "";
    
    @EJB
    private RegisterSessionBeanLocal registerBean;
    
    public String register() {
        
        boolean result = registerBean.registerKorisnika(name, surname, username, password, user);
        
        if (!result) {
            message = "Greška prilikom registracije. Korisničko ime je verovatno zauzeto.";
            return "failure";
        }
        
        message = "";
        return "success";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }
}
