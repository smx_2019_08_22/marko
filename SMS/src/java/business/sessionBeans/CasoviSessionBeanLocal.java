package business.sessionBeans;

import entities.Casovi;
import java.util.List;
import javax.ejb.Local;

@Local
public interface CasoviSessionBeanLocal {
    
    public List<Casovi> getAllCasovi();
}
