package business.sessionBeans;

import entities.Casovi;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class CasoviSessionBean implements CasoviSessionBeanLocal {

    @PersistenceContext(unitName = "SMSPU")
    private EntityManager em;
    @Override
    public List<Casovi> getAllCasovi() {
        try {
            Query q = em.createNamedQuery("Casovi.findAll");
            List<Casovi> casovi = q.getResultList();
         
            return casovi;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
