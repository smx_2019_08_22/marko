package business.sessionBeans;

import entities.Ocene;
import java.util.List;
import javax.ejb.Local;
@Local
public interface OceneSessionBeanLocal {
    public List<Ocene> getOceneUcenika(Integer ucenik);
    public List<Ocene> getOceneWhereUcenikAndPredmet(Integer ucenik, Integer predmet);
}
